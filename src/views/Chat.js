import React from 'react';
import {View, Text, StyleSheet, FlatList, TextInput, AsyncStorage} from 'react-native';
import { Navigation } from 'react-native-navigation';
import defaultStyles from '../res/styles';
import strings from '../res/strings';
import Socket from '../util/socket';
import Base64 from '../util/base64';


class Chat extends React.Component{

	static navigatorStyle = defaultStyles.navbar;

	static navigatorButtons = {
		rightButtons: [
			{
				title: strings.logout,
				id: 'logout',
				buttonColor: "#fff",

			}
		]
	};


	state = {
	    history: [],
	    text: '',
	};

	constructor(props){
		super(props);
		this.onMessageReceived = this.onMessageReceived.bind(this);
	    this.onSendMessage = this.onSendMessage.bind(this);
	    this.onChangeText = this.onChangeText.bind(this);

	    this._renderMessage = this._renderMessage.bind(this);

	    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}
	async onNavigatorEvent(event){
		switch(event.id){
			case 'logout':
				try{
					await AsyncStorage.clear();
					Navigation.startSingleScreenApp({
						screen: {
							screen: 'login',
							title: 'Login',
							navigatorStyle: {
								navBarHidden: true
							},
						},
					});

				}catch(error){
					console.log("logout_error", error);
					alert(strings.logout_error);
				}
			break;
			case 'willAppear':
				this.socket = Socket.getInstance(this.onMessageReceived);
   			break;

			case 'willDisappear':
        		this.socket.callback  = null;
        	break;
		}

	}

	onMessageReceived(response){

		if(response && response.result, response.type == "MESSAGE"){
			const {data} = response;
			const user = JSON.parse(Base64.b64DecodeUnicode(data.user));
			const message = {
				msg: data.text,
				name: user.name,
				owner: this.token == data.user
			};
			this.setState({
		      history: [
		        ...this.state.history,
		        message,
		      ]
		    });
		}
		

		


	    

	    setTimeout(() => {
	    	if(this && this.list){
	    		this.list.scrollToEnd();
	    	}
		   
		}, 50);
	}

	onSendMessage(){
	    const {text} = this.state;
	    if(text.trim() != ""){
	    	const type = "MESSAGE";
		    const payload = {user: this.token, text: text};
		    this.socket.performAction(type, payload);
	    }
	    this.setState({text: ''});
	}

	onChangeText( text ){
	    this.setState({ text });
	}

	_renderMessage({item, index}){
	    const kind = item.owner ? styles.me: styles.friend;
	    return (
	      <View style={[styles.msg, kind]} key={index}>
	      	<Text style={styles.msg_sender}>{!item.owner ? item.name : null}</Text>
	        <Text style={styles.msg_body}>{item.msg}</Text>
	      </View>
	    );    
	}


	
	async componentWillMount(){
		const token = await AsyncStorage.getItem('token');
		this.token = token;
	}
	render(){
		let {history, text} = this.state;
		return(
			<View style={defaultStyles.container}>
				<FlatList
					contentContainerStyle={{flexGrow: 1}}
					ref={ref => this.list = ref}
					keyExtractor={(item, index)=> `chat_${index}`}
					data={[...history]}
					renderItem={this._renderMessage.bind(this)}
				>
				</FlatList>
		        <View style={defaultStyles.inputContainer}>
		          <TextInput 
		            style={defaultStyles.input}
		            value={text}
		            onChangeText={this.onChangeText}
		            onSubmitEditing={this.onSendMessage}
		          />
		        </View>
	      	</View>
		);
	}

}

const styles = StyleSheet.create({
  toolbar: {
    backgroundColor: '#34495e',
    color: '#fff',
    fontSize: 20,
    padding: 25,
    textAlign: 'center',
  },
  content: {
    flex: 1,
  },
  msg: {
    margin: 5,
    padding: 12,
    borderRadius: 5,
  },
  msg_sender: {
  	color: "#fff",
  	fontSize: 12, 
  },
  msg_body: {
  	color: "#000",
  	fontSize: 16,
  },
  friend: {
    alignSelf: 'flex-start',
    backgroundColor: '#1abc9c',
    marginRight: 100,
  },
  me: {
    alignSelf: 'flex-end',
    backgroundColor: '#fff',
    marginLeft: 100,
  },
});

export default Chat;