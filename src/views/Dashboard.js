import React from 'react';
import {View, Text, TouchableOpacity, AsyncStorage, ActivityIndicator} from 'react-native';

import defaultStyles from '../res/styles';
import strings from '../res/strings';

import Socket from '../util/socket';
import Config from '../util/config';




class Dashboard extends React.Component{
	static navigatorStyle = defaultStyles.navbar;

	state = {
		loading: false,
	};

	constructor(props){
		super(props);

		this.onCreateMatchPerformed = this.onCreateMatchPerformed.bind(this);
		this.performCreateMatch = this.performCreateMatch.bind(this);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event){
		switch(event.id){
			case 'willAppear':
				this.socket = Socket.getInstance(this.onCreateMatchPerformed);
   			break;

			case 'willDisappear':
        		this.socket.callback  = null;
        	break;
		}
	}

	onCreateMatchPerformed(data){
		if(data && data.result && data.type == "CREATE_GAME"){
			this.props.navigator.push({
				screen: 'app.lobbie',
				title: strings.lobbie_title,
				overrideBackPress: true,
			});
		}else{
			if(data && data.type == "CREATE_GAME"){
				alert(strings.create_game_error);
			}
		}
		setTimeout(()=>{
			this.setState({loading: false});
		}, 500);
		
	}

	performCreateMatch(){
		const token = Config.getToken();
		const type = "CREATE_GAME";
		const payload = {token};
		this.socket = Socket.getInstance(this.onCreateMatchPerformed);
		this.socket.performAction(type, payload);
		this.setState({loading: true});
	}




	render(){
		let {loading} = this.state;
		return(
			<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
				{loading ?
					<ActivityIndicator size="large" color="#000"></ActivityIndicator>
				:
					<View style={{flex: 1, marginTop: 100, marginBottom: 100, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-around'}}>
						<TouchableOpacity onPress={this.performCreateMatch}>
							<View style={[defaultStyles.button_big, {backgroundColor: '#351c75'}]}>
								<Text style={{color: '#fff', fontSize: 18,}}>{strings.create_match}</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={()=>{
							this.props.navigator.push({
								screen: 'app.games',
								title:  strings.games,
							});
						}}>
							<View style={[defaultStyles.button_big, {backgroundColor: '#741b47'}]}>
								<Text style={{color: '#fff', fontSize: 18,}}>{strings.join_match}</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={()=>{
							this.props.navigator.push({
								screen: 'app.chat',
								title:  strings.chat,
							});
						}}>
							<View style={[defaultStyles.button_big, {backgroundColor: '#085394'}]}>
								<Text style={{color: '#fff', fontSize: 18,}}>{strings.chat}</Text>
							</View>
						</TouchableOpacity>
					</View>
				}
				
			</View>
		);
	}
}


export default Dashboard;