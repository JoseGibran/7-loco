import React from 'react';
import {View, Text, TextInput, TouchableOpacity, Image, AsyncStorage, ActivityIndicator, Dimensions} from 'react-native';
import {Navigation} from 'react-native-navigation';

import defaultStyles from '../res/styles';
import strings from '../res/strings';

import Socket from '../util/socket';
import Config from '../util/config';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class Login extends React.Component{

	state = {
		email: '',
		password: '',
		loading: true, 
	};

	constructor(props){
		super(props);
		this.performLogin = this.performLogin.bind(this);
		this.onLoginPerformed = this.onLoginPerformed.bind(this);
		this.checkForOldUser = this.checkForOldUser.bind(this);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));

	    Socket.init();
	    this.socket = Socket.getInstance(()=>{});
		this.socket.ws.onopen = ()=>{
			this.checkForOldUser();
		}
	}

	onNavigatorEvent(event){
		switch(event.id){
			case 'willAppear':
				this.socket = Socket.getInstance(this.onLoginPerformed);
   			break;

			case 'willDisappear':
        		this.socket.callback  = null;
        	break;
		}
	}

	performLogin(){
		const {email, password} = this.state;
		const type = "LOGIN";
		const payload = {email, password};
		this.socket = Socket.getInstance(this.onLoginPerformed);
		this.socket.performAction(type, payload);
		this.setState({loading: true});
	}

	async onLoginPerformed(data){
		if(typeof data.result !== 'undefined'){
			if(data.result){
				try{
					await AsyncStorage.setItem('token', data.token);
					Config.setToken(data.token);
					Navigation.startSingleScreenApp({
						screen: {
							screen: 'app.dashboard',
							title: strings.dashboard_title,
						},
						
					});
				}catch (error){
					alert(strings.login_error);
				}
			}else{
				alert(strings.invalid_login);
			}
		}		
		setTimeout(() => {
			this.setState({loading:false});
		}, 1000)
	}	


	async checkForOldUser(){
		let page;
		let token;
		try{
			token = await AsyncStorage.getItem('token') ;
			setTimeout(()=>{
				this.setState({loading:false});
			}, 500)
			if(token){
				Config.setToken(token);

				this.socket = Socket.getInstance((response) => {
					if(response && response.result && response.type == "RESET_USER"){
						Navigation.startSingleScreenApp({
							screen: {
								screen: 'app.dashboard',
								title: strings.dashboard_title,
							},
							
						});
					}else{
						alert(strings.error_reset_user);
					}
				});
				
				const type = "RESET_USER";
				const payload = {token};
				this.socket.performAction(type, payload)	
			}else{
				this.socket = Socket.getInstance(this.onLoginPerformed);
			}
			
		}catch(error){
			console.log("asyncStorage_error", error);
			alert(strings.async_storage_error);
		}
	}
	render(){	
		let {email, password, loading} = this.state;

		const containerStyle = [defaultStyles.container, {flexDirection: 'row', alignItems: 'center', justifyContent: 'center'} ];
		const backgroundImageStyle = {width: SCREEN_WIDTH, height: SCREEN_HEIGHT};

		return (
			<View style={containerStyle}>
				<View style={styles.overlay}>
					
					<Image 
						source={require('../assets/login_bg.png')}
						style={backgroundImageStyle}
					/>
				</View>
					{loading ? 
						<ActivityIndicator size="large" color="#fff"></ActivityIndicator>
					:
						<View style={{flex: 1, marginLeft: 50, marginRight: 50, backgroundColor: "rgba(0,0,0,0.5)", padding: 25,}}>
							<Text style={{color: "#fff", fontSize: 40,}}>{strings.welcome}</Text>
							<TextInput onChangeText={email=>{this.setState({email})}} value={email} style={styles.input} placeholder={strings.email} placeholderTextColor={"#fff"} underlineColorAndroid={"#fff"}/>
							<TextInput  onChangeText={password=>{this.setState({password})}} value={password} style={styles.input} placeholder={strings.password} placeholderTextColor={"#fff"} underlineColorAndroid={"#fff"} secureTextEntry={true}/>
							<TouchableOpacity style={defaultStyles.button}  onPress={this.performLogin}>
								<Text style={{color: '#000', fontSize: 18,}}>{strings.go}</Text>
							</TouchableOpacity>
						</View>
					}
			</View>
		)
	}
}

const styles = {
	input: {
		marginBottom: 10,
		fontSize: 18,
		padding: 10,
		color: "#fff",
	},
	overlay:{
		position: 'absolute',
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor: '#000',
		opacity: 0.7,
	},
	title: {
		color: "#fff", position: 'absolute', top: 60, fontSize: 30,
		textShadowColor: 'rgba(0, 0, 0, 0.75)',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 10
	}
}

export default Login;