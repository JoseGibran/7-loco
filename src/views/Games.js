import React from 'react';
import {View, Text, FlatList, TouchableNativeFeedback, Alert, AsyncStorage} from 'react-native';

import defaultStyles from '../res/styles';
import strings from '../res/strings';

import Socket from '../util/socket';

class Games extends React.Component{

	static navigatorStyle = defaultStyles.navbar;

	state ={
		loading: false,
		games: [],
	};

	constructor(props){
		super(props);
		this.getInitialData = this.getInitialData.bind(this);
		this.onGetGames = this.onGetGames.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this.join = this.join.bind(this);

		this.performJoinGame = this.performJoinGame.bind(this);
		this.onPerformedJoinGame = this.onPerformedJoinGame.bind(this);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}
	onNavigatorEvent(event){
		switch(event.id){
			case 'willAppear':
				this.socket = Socket.getInstance(this.onGetGames);
				this.getInitialData();
   			break;

			case 'willDisappear':
        		this.socket.callback  = null;
        	break;
		}
	}

	getInitialData(){
		const type = "GET_CREATED_GAMES";
		const payload = {};
		this.socket = Socket.getInstance(this.onGetGames);
		this.socket.performAction(type, payload);	
	}

	onGetGames(response){
		if(response && response.result){
			if(response.type == "GET_CREATED_GAMES");

			let games = [];
			response.data.forEach((game, index) =>{
				if(!game.started){
					games = [...games, game];
				}
			});

			this.setState({games});
		}else{
			this.setState({games: []});
		}
	}

	join(game){
		Alert.alert(
		  strings.games_alert_title,
		  `${strings.games_alert_body}${game.name}`,
		  [
		    {text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
		    {text: strings.ok, onPress: this.performJoinGame.bind(this, game) },
		  ],
		  { cancelable: false }
		)
	}

	async performJoinGame(game){
		const token = await AsyncStorage.getItem('token');
		this.socket.callback = this.onPerformedJoinGame; // Change the callback listener ;)
		const type = "JOIN_GAME";
		const payload = {token: token, emailGame: game.email};
		this.socket.performAction(type, payload);

	}
	onPerformedJoinGame(data){
		if(data && data.result && data.type == "JOIN_GAME"){
			this.props.navigator.push({
				screen: 'app.lobbie',
				title: strings.lobbie_title,
				overrideBackPress: true,
			});
		}else{
			if(data && data.type == "JOIN_GAME"){
				alert(strings.games_join_error);
			}	
		}
	}


	_renderRow({item, index}){
		return(
	 	<TouchableNativeFeedback
		        onPress={this.join.bind(this, item)}
		        background={TouchableNativeFeedback.SelectableBackground()}>
		    <View style={defaultStyles.row}>
				<Text style={defaultStyles.row_text}>{item.name}</Text>
				<Text style={defaultStyles.row_text}>{item.players}</Text>
			</View>
	    </TouchableNativeFeedback>
		);
	}

	render(){
		let {games} = this.state;
		return(
			<View>
				<View style={[defaultStyles.row, {backgroundColor: '#34495e'}]}>
					<Text style={[defaultStyles.row_text, {color: '#fff', fontWeight: 'bold'}]}>{strings.games_title_1}</Text>
					<Text style={[defaultStyles.row_text, {color: '#fff', fontWeight: 'bold'}]}>{strings.games_title_2}</Text>
				</View>
				<FlatList 
					data={[...games]}
					renderItem={this._renderRow}
					keyExtractor={(value, index) => index}
				/>
			</View>
		);
	}
}

const styles = {
	
};

export default Games;