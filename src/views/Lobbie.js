import React from 'react';
import {View, Text, AsyncStorage, TouchableNativeFeedback, FlatList, TouchableOpacity, BackHandler} from 'react-native';

import Socket from '../util/socket';
import Config from '../util/config';
import defaultStyles from '../res/styles';
import strings from '../res/strings';

class Lobbie extends React.Component{

	state = {
		players: [],
		host: '',
		loading: false,
	};

	static navigatorButtons = {
	    leftButtons: [{
	      id: 'back'
	    }],
	    animated: true
  	};

	constructor(props){
		super(props);
		this.onGetInfo = this.onGetInfo.bind(this);
		this.leaveLobbie = this.leaveLobbie.bind(this);
		this.startGame = this.startGame.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event){
		switch(event.id){
			case 'willAppear':
				this.socket = Socket.getInstance(this.onGetInfo);
				this.getInitialData();

				BackHandler.addEventListener('hardwareBackPress', function() {
				   if(this.leaveLobbie){this.leaveLobbie();} 
				    return false;
			  	});

   			break;

			case 'willDisappear':
        		this.socket.callback  = null;
        		BackHandler.removeEventListener('hardwareBackPress', function() {});
        	break;

        	case 'backPress': //insted of onBack 
        		if(this.leaveLobbie){this.leaveLobbie();}
        		this.props.navigator.pop();
        	break;
		}
	}
	getInitialData(){
		const token = Config.getToken();
		const type = "GET_LOBBIE_INFO";
		const payload = {token};
		this.socket.performAction(type, payload);
	}

	onGetInfo(response){
		if(response && response.result && response.type == "GET_LOBBIE_INFO"){
			this.setState({players: response.data.players, host:  response.data.email});
		}
		if(response && response.result && response.type == "START_GAME"){

			this.setState({loading: false});

			this.props.navigator.push({
				screen: 'app.game',
				title: 'Juego',
				navigatorStyle: {
				 	tabBarHidden: true,
				 	navBarHidden: true,
				}
			});
		}
		
	}

	leaveLobbie(){
		const token = Config.getToken();
		const type = "LEAVE_LOBBIE";
		const payload = {token};
		this.socket.performAction(type, payload);
	}
	startGame(){
		const currentUser = Config.getCurrentUser();
		const token = Config.getToken();
		let {players, host} = this.state;
		console.log(players.length);
		if(host == currentUser.email){
			//if(players.length > 1){
				this.socket = Socket.getInstance(this.onGetInfo);
				const type = "START_GAME";
				const payload = {token};
				this.socket.performAction(type, payload);

			// }else{
			// 	alert(strings.no_enough_players);
			// }	
		}else{
			alert(strings.no_host);
		}
	}


	_renderRow({item, index}){
		const {host} = this.state;
		return(
		 	<TouchableNativeFeedback

			        onPress={()=>{}}
			        background={TouchableNativeFeedback.SelectableBackground()}>
			    <View style={defaultStyles.row}>
					<Text style={defaultStyles.row_text}>{item.name}</Text>
					{host == item.email ?
						<Text style={styles.host_label}>{strings.host}</Text>
					: null }
				</View>
		    </TouchableNativeFeedback>
		);
	}
	render(){
		let {players, loading} = this.state;
		return(
			<View style={{flexDirection: 'column', flex: 1,}}>
				<FlatList 
					data={[...players]}
					renderItem={this._renderRow}
					keyExtractor={(value, index) => index}
				/>	
				<View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
					<TouchableOpacity onPress={()=>{
						this.leaveLobbie();
						this.props.navigator.pop();

					}} style={[defaultStyles.button, {backgroundColor: '#D8000C', flexGrow: 1,}]}>
						<Text style={{color: '#fff', fontSize: 18,}}>{strings.leave_lobbie}</Text>
					</TouchableOpacity>

					<TouchableOpacity onPress={()=>{
						this.props.navigator.push({
							screen: 'app.chat',
							title: strings.chat,
						})
					}} style={[defaultStyles.button, {backgroundColor: '#428BCA', flexGrow: 1}]}>
						<Text style={{color: '#fff', fontSize: 18,}}>{strings.chat}</Text>
					</TouchableOpacity>

					<TouchableOpacity disabled={loading} onPress={this.startGame} style={[defaultStyles.button, {backgroundColor: '#4F8A10', flexGrow: 1,}]}>
						<Text style={{color: '#fff', fontSize: 18,}}>{strings.start_game}</Text>
					</TouchableOpacity>
				</View>
				
			</View>
		);
	}

}

const styles = {
	host_label: {fontSize: 10, color: "#fff", backgroundColor: "#4F8A10", padding: 5}
}

export default Lobbie;