import React from 'react';

import ReactNative, {View, Text, TouchableOpacity, FlatList, Vibration } from 'react-native';

import Card from '../components/Card';
import BackCard from '../components/BackCard';
import ProgressBar from '../components/ProgressBar';
import PopRequest from '../components/PopRequest';
import Loading from '../components/Loading';
import PopupContainer from '../components/PopupContainer';
import PopAlert from '../components/PopAlert';

import defaultStyles from '../res/styles';
import strings from '../res/strings';
import GameStack from '../util/game';

import Socket from '../util/socket';
import Config from '../util/config';

var RCTUIManager = require('NativeModules').UIManager;

class Game extends React.Component{
	static navigatorStyle = {
        tabBarHidden: true
    };

    state = {
    	players: [
    		{name: "INIT", cards: [{team: 1, number:1}]}, 
    	],
    	currentPlayerIndex: 0,
    	currentTurn: 0,
    	cardsToPull: 0,
    	oneDroper: null,
    	currentCard: {number: 1, team:1},
    	loading: true,

    };

	constructor(props){
		super(props);
		this.turnOver = this.turnOver.bind(this);
		this.onCardDroped = this.onCardDroped.bind(this);
		this.requestCard = this.requestCard.bind(this);
		this.changeColor = this.changeColor.bind(this);
		this.skypeTurn = this.skypeTurn.bind(this);
		this.performLastCard = this.performLastCard.bind(this);
		this.performChallenge = this.performChallenge.bind(this);

		this.onGameEvent = this.onGameEvent.bind(this);

		this._renderPlayer = this._renderPlayer.bind(this);
		this._renderCard = this._renderCard.bind(this);

		this.selectedCard = null;

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}
	onNavigatorEvent(event){
		switch(event.id){
			case 'willAppear':
				this.socket = Socket.getInstance(this.onGameEvent);
				//this.socket = { performAction: ()=>{}};
   			break;

			case 'willDisappear':
				if(this.socket){
					this.socket.callback  = null;
				}
        	break;
		}
	}

	turnOver(){
		//alert("YOUR TURN IS OVER, TAKE TWO CARDS");
	}
	onCardDroped(card){


		const {currentCard, players, currentTurn, cardsToPull, oneDroper, currentPlayerIndex}  = this.state;
		this.selectedCard = card;



		if(currentTurn === currentPlayerIndex){
			//PROTOCOL A
			if(currentCard.number == 1){
				if(oneDroper && oneDroper == players[currentTurn].name){ // WILL REPLACE WITH TOKEN ON WEB SOCKET

					//PROTOCOL 7
					if(card.number == 7){
						players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
						this.setState({players: players}, ()=>{
							
							this._popRequest.open();
						});
					}else{
						if(card.number == currentCard.number || card.team == currentCard.team){
							const type = "DROP_CARD";
							const payload = {
								token: Config.getToken(),
								card: card,
								type: type,
							};
							//DELETE THE LOCAL CARD FIRST
							players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
							this.setState({players}, ()=>{
								this.socket.performAction(type,payload);
							});
						}else{

							//alert("NO SEA PENDEJO --'");
						}

					}

					
				}else{
					//NO ONE ACTION
					if(card.number == currentCard.number){
						const type = "DROP_CARD";
						const payload = {
							token: Config.getToken(),
							card: card,
							type: type,
						};
						players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
						this.setState({players}, ()=> {
							this.socket.performAction(type,payload);
						});
					}else{
						alert(`Cuando hay un As solo un As se puede colocar`);
					}

				}
			}else{
				//PROTOCOL 2
				if(currentCard.number == 2 && cardsToPull > 0){
					if(card.number == 2){
						const type = "DROP_CARD";
						const payload = {
							token: Config.getToken(),
							card: card,
						};

						//DELETE THE LOCAL CARD FIRST
						players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
						this.setState({players}, ()=>{
							this.socket.performAction(type,payload);
						});
					}else{
						alert("CUANDO HAY UN 2 SOLO SE SALVA CON OTRO 2");
					}
				}else{

					//PROTOCOL 7
					if(card.number == 7){
						players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
						this.setState({players: players}, ()=>{
							this._popRequest.open();
						});
					}else{
						if(card.number == currentCard.number || card.team == currentCard.team){
							const type = "DROP_CARD";
							const payload = {
								token: Config.getToken(), //REPLACE WITH TOKEN ON WEB SOCKET
								card: card,
							};
							//DELETE THE LOCAL CARD FIRST
							players[currentPlayerIndex].cards[this.getCardIndex(card)].hidden = true;
							this.setState({players}, ()=>{
								this.socket.performAction(type,payload);
							});
						}else{
							//alert("NO SEA PENDEJO --'");
						}
					}

					
				}
			}
		}else{
			alert(strings.invalid_turn);
		}

	}

	requestCard(){
		const {players, currentTurn, currentPlayerIndex} = this.state;
		if(currentTurn === currentPlayerIndex){
			const type = "REQUEST_CARD";
			const payload = {
				token: Config.getToken()
			};
			this.socket.performAction(type,payload);
		}else{
			alert(strings.invalid_turn);
		}
		
	}

	changeColor(team){
		//PROTOCOL 7
		const {players, currentTurn} = this.state;
		const type = "DROP_CARD";
		const payload = {
			token: Config.getToken(), //REPLACE WITH TOKEN ON WEB SOCKET
			oldCard: this.selectedCard,
			newCard: {...this.selectedCard, team: team},
		};
		this.setState({players}, ()=>{
			this.socket.performAction(type,payload);
		});
	}

	skypeTurn(){
		const {players, currentTurn, cardsToPull, currentPlayerIndex} = this.state;

		if(currentPlayerIndex === currentTurn){
			const type = "SKYPE_TURN";
			const payload = {
				token: Config.getToken(),
			};
			this.socket.performAction(type, payload);
		}else{
			alert(strings.invalid_turn);
		}
	}
	performLastCard(){
		const {players, currentPlayerIndex} = this.state;
		if(players[currentPlayerIndex].cards.length === 1){
			const type = "LAST_CARD";
			const payload = {
				token: Config.getToken(),
			};
			this.socket.performAction(type, payload);
		}	
	}
	performChallenge(){
		const type = "CHALLENGE";
		const payload = {
			token: Config.getToken(),
		};
		this.socket.performAction(type, payload);
	}


	onGameEvent(event){

		console.log(event);
		let {currentPlayerIndex} = this.state;
		if(event && event.result){
			switch(event.type){
				case "GAME_STARTED":
					event.data.players.forEach((player, index) => {
						if(player.email == Config.getCurrentUser().email){
							currentPlayerIndex = index;
						}
					});
					this.setState({
						loading: false,
						currentPlayerIndex: currentPlayerIndex,
						players: event.data.players,
						currentCard: event.data.currentCard,
						currentTurn: event.data.currentTurn,
					});
				break;
				case "DROP_CARD":
					this.setState({
						players: event.data.players,
						currentTurn: event.data.currentTurn,
						currentCard: event.data.currentCard,
						cardsToPull: event.data.cardsToPull,
						oneDroper: event.data.oneDroper,
					}, ()=>{
						Vibration.vibrate(250);
						this._popupContainer.start();
					});
				break;
				case "REQUEST_CARD":
					this.setState({
						players: event.data.players,
					});
				break;
				case "SKYPE_TURN":
					this.setState({
						currentTurn: event.data.currentTurn,
						players: event.data.players,
						cardsToPull: event.data.cardsToPull,
						oneDroper: event.data.oneDroper,
					});
				break;

				case "GAME_OVER":
					alert(`Ha ganado: ${event.data.winner}`);
					this.props.navigator.popToRoot();
				break;

				case "LAST_CARD":
					this._popAlert.state.title = strings.last_card;
					this._popAlert.state.subtitle = event.data.player;
					this._popAlert.show();
				break;

				case "CHALLENGE":
					this._popAlert.state.title = strings.challenged_by;
					this._popAlert.state.subtitle = event.data.player;
					this._popAlert.show();
				break;

				case "UPDATE_PLAYERS":
					this.setState({
						players: event.data.players,
					});
				break;


				case "NO_MORE_CARDS":
					alert(strings.NO_MORE_CARDS);
				break;
			}
		}
	}

	_renderPlayer({item, index}){
		const {currentTurn, currentPlayerIndex} = this.state;
		const activeTextStyle = currentTurn === currentPlayerIndex ? {color: "#fff"} : {};
		return (
			<View style={styles.player}>
				<Text style={[activeTextStyle]}>{item.name}</Text>
				{item.cards.length === 1 ?
					<Text style={[activeTextStyle]}>{item.cards.length}</Text>
				: null}
			</View>
		);
	}
	_renderCard(card, index){
		let {dropZoneValues} = this.state;
		return(
			<Card key={`card_${index}`} dropZoneValues={dropZoneValues} onCardDroped={this.onCardDroped} number={card.number} team={card.team} hidden={card.hidden}/>
		);
	}

  	_keyExtractor = (item, index) => index;
	render(){
		const {players, currentTurn, currentCard, currentPlayerIndex, loading} = this.state;
		const activeStyle = currentTurn === currentPlayerIndex ? {backgroundColor: '#000' } : {};
		const activeTextStyle = currentTurn === currentPlayerIndex ? {color: "#fff"} : {};

		return loading ? 
		(<Loading />)
		:
		(
			<View style={[styles.container, activeStyle]}>
				<View style={styles.header}>
					<View>
						<Text style={styles.title}>{`${strings.games_title_2}:`}</Text>
						<FlatList 
							data={[...players]}
							keyExtractor={this._keyExtractor}
							renderItem={this._renderPlayer}
						/>
					</View>
					<View style={{width: 150}}>
						<Text style={[styles.title, activeTextStyle]}>{strings.current_turn}</Text>
						<Text style={[styles.current_turn, activeTextStyle]}>{players[currentTurn].name}</Text>
						<ProgressBar width={150} time={5000} onFinish={this.turnOver} />
					</View>
						
					
				</View>
					
				<View style={styles.table}>
					<BackCard onPress={this.requestCard.bind(this)} size={150}/>
					<PopupContainer ref={ ref => this._popupContainer = ref}>
						<View onLayout={()=>{
							let handle = ReactNative.findNodeHandle(this.card); 
							RCTUIManager.measure(handle, (x, y, width, height, pageX, pageY) => {
								this.setState({dropZoneValues: {pageX, pageY, width, height}});
							});
						}} ref={ref => this.card = ref} >
							
							<Card number={currentCard.number} team={currentCard.team} size={150} dragging={false}/>
						</View>
					</PopupContainer>
					
				</View>	
				<View style={styles.hand}>
					{players[currentPlayerIndex].cards.map(this._renderCard)}			
				</View>

				<View style={styles.hand}>
					<TouchableOpacity onPress={this.performChallenge} style={[defaultStyles.button, {backgroundColor: '#D8000C', flexGrow: 1,}]}>
						<Text style={styles.button_text}>{strings.dear}</Text>
					</TouchableOpacity>

					<TouchableOpacity onPress={this.skypeTurn} style={[defaultStyles.button, {backgroundColor: '#0275d8', flexGrow: 1,}]}>
						<Text style={styles.button_text}>{strings.skype_turn}</Text>
					</TouchableOpacity>


					<TouchableOpacity onPress={this.performLastCard} style={[defaultStyles.button, {backgroundColor: '#4F8A10', flexGrow: 1,}]}>
						<Text style={styles.button_text}>{strings.last_card}</Text>
					</TouchableOpacity>
				</View>

				<PopRequest onOptionSelected={this.changeColor} ref={(pop) => this._popRequest = pop}/>
				<PopAlert ref={(pop) => this._popAlert = pop}/>

			</View>
		);
	}

	getCardIndex = (card) => {
		const {players, currentPlayerIndex}  = this.state;
		const {cards} = players[currentPlayerIndex];
		let indexWillReturn = null;
		cards.forEach((value, index) => {
			if(!indexWillReturn){
				if(value.number === card.number && value.team === card.team ){
					indexWillReturn = index; 
				}
			}
		});
		return indexWillReturn;
	}
}

const styles = {
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',
		backgroundColor: "#E6E7E8",
	},
	header:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 20,
		paddingRight: 20,
		height: 100,
		borderWidth: 1,
		alignItems: 'center',
		borderColor: "#7C7C7C",
	},
	title:{
		fontSize: 16,
		fontWeight: 'bold',
	},
	currentTurn:{
		fontSize: 25,

	},
	table: {
		paddingLeft: 20,
		paddingRight: 20,
		justifyContent: 'space-around',
		flexDirection: 'row'
	},
	hand: {
		paddingLeft: 20,
		paddingRight: 20,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-around'
	},

	player: {
		paddingRight: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	button_text: {
		color: '#fff', fontSize: 18,
	},
}

export default Game;