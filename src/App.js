
import React from 'react';

import registerScreens from './navigation/registerScreens';
import mainStack from './navigation/mainStack';

import socket from './util/socket';


class App {
  
  constructor(){

    registerScreens();
    mainStack();
  }

}

export default App;

