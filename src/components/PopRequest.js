import React from 'react';
import {View, Text, Animated, Dimensions, Image, TouchableOpacity, Easing} from 'react-native';
import strings from '../res/strings';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class PopRequest extends React.Component{

	static defaultProps = {
		onOptionSelected: () => {},
		show: false,
	};
	constructor(props){
		super(props);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.onOptionSelected = this.onOptionSelected.bind(this);

		this.state = {
			show: this.props.show,
			opacityAnim: new Animated.Value(0.01),
		}
	}

	open(){
		let {opacityAnim} = this.state;
		this.setState({show: true}, ()=>{
			Animated.timing(
		        opacityAnim,
		        {
		          toValue: 1,
		          duration: 250,
		          useNativeDriver: true,
		        }
		    ).start();
		});
		
		
	}

	close(){
		let {opacityAnim} = this.state;
		Animated.timing(
			opacityAnim,
			{
				toValue:0,
				duration: 250,
				useNativeDriver: true,
			}
		).start(()=>{
			this.setState({show: false});
		});
	}

	onOptionSelected(team){
		this.close();
		this.props.onOptionSelected(team);
	}

	render(){
		let {show} = this.state;
		return show ? 
		(
			<View style={styles.backdrop}>
					<Animated.View  style={[styles.container, {transform: [
						{scaleX: this.state.opacityAnim},
						{scaleY: this.state.opacityAnim},
					]}]}>
						<TouchableOpacity onPress={this.onOptionSelected.bind(this, 1)}>
							<View style={[styles.item, {borderRightWidth: 1, borderBottomWidth: 1,}]}>
								<Image style={styles.image}  resizeMode={"contain"} source={require('../assets/diamond.png')}/>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.onOptionSelected.bind(this, 3)}>
							<View style={[styles.item, {borderBottomWidth: 1,}]}>
								<Image style={styles.image} resizeMode={"contain"} source={require('../assets/spade.png')}/>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.onOptionSelected.bind(this, 4)}>
							<View style={[styles.item, {borderRightWidth: 1,}]}>
								<Image style={styles.image} resizeMode={"contain"} source={require('../assets/trebor.png')}/>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.onOptionSelected.bind(this, 2)}>
							<View style={[styles.item]}>
								<Image style={styles.image} resizeMode={"contain"} source={require('../assets/heart.png')}/>
							</View>
						</TouchableOpacity>
					</Animated.View>
				</View>
		) : null;
	}
}
const styles = {
	backdrop:{
		top:0,
		bottom: 0,
		right:0,
		left:0,
		position: 'absolute',
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		elevation: 4,
	},
	container: {
		width: 200,
		height: 200,
		borderRadius: 5,
		padding: 5,
		backgroundColor: "#fff",
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	item: {
		width: 90,
		height: 90,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: "#7C7C7C"
	},
	image: {
		width: 80,
		height: 80,
	}
}

export default PopRequest;