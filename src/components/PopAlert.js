import React from 'react';
import {View, Text, Animated, Dimensions, Image, TouchableOpacity, Easing} from 'react-native';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class PopAlert extends React.Component {

	static defaultProps = {
		show: false,
		title: 'TITLE',
		subtitle: 'SUBTITLE',
	};

	constructor(props){
		super(props);

		this.state = {
			show: this.props.show,
			scaleAnim : new Animated.Value(0.01),
			title: this.props.title,
			subtitle: this.props.subtitle,
		}
	}

	show = (callback) =>{
		const {scaleAnim} = this.state;
		this.setState({show: true}, ()=> {
			Animated.timing(
				scaleAnim,
				{
					toValue: 1.2,
					duration: 250,
				}
			).start(()=> {
				setTimeout(()=> {
					Animated.timing(
						scaleAnim,
						{
							toValue: 0.01,
							duration: 250,
						}
					).start(()=>{
						this.setState({show: false}, callback);
					});
				}, 750)
			});
		});
	}


	render(){
		const {show, scaleAnim} = this.state;
		const {title, subtitle} = this.state;
		const containerStyle = [styles.container, {transform: [{scaleX: scaleAnim}, {scaleY: scaleAnim}]}];
		return show ? (
			<View style={styles.backdrop}>
				<Animated.View  style={containerStyle}>
					<Text style={styles.title}>{title}</Text>
					<Text style={styles.subtitle}>{subtitle}</Text>
				</Animated.View>
			</View>
			
		) : null;
	};
}

const styles = {
	backdrop:{
		top:0,
		bottom: 0,
		right:0,
		left:0,
		position: 'absolute',
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		elevation: 4,
	},
	container: {
		borderRadius: 5,
		paddingTop: 15,
		paddingBottom: 15,
		paddingRight: 5,
		paddingLeft: 5,
		backgroundColor: "#fff",
		justifyContent: 'center',
    	alignItems: 'center',
		flexDirection: 'column',
	},
	title: {
		textAlign: 'center',
		color: "#0275d8",
		fontSize: 24,
	},
	subtitle: {
		textAlign: 'center',
		fontSize: 20,
	}
}

export default PopAlert;