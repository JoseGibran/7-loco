import React from 'react';
import {View, Image, TouchableWithoutFeedback} from 'react-native';


class BackCard extends React.Component{

	static defaultProps = {
		size: 70,
		onPress: () => {}
	};

	constructor(props){
		super(props);

		this.state = {
			width: this.props.size,
			height: this.props.size * 1.285714285,
		}
	}

	render(){

		const {width, height} = this.state;

		return(
			<TouchableWithoutFeedback onPress={this.props.onPress}>
				<View style={[styles.container, {width, height}]}>
					<Image style={[styles.image, {width, height}]} source={require('../assets/back_card.png')}/>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	container:{
		width: 70,
		height: 90,
	},
	image:{
		width: 70,
		height: 90,
		borderRadius: 5,
		shadowOffset:{  width: 10,  height: 10,  },
		shadowColor: 'black',
		shadowOpacity: 1.0,
	}
}

export default BackCard;
