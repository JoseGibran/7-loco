	import React from 'react';
import {View, Text, Image, PanResponder, Animated} from 'react-native';

const RED = "#E61408";
const BLACK = "#000";

class Card extends React.Component{

	static defaultProps ={
		number: 1,
		team: 1, /*
			1... Diamantes,
			2... Corazones Rojos,
			3... Corazones Negros,
			4... Treboles
		*/
		size: 70,
		dragging: true,
		dropZoneValues: null,
		hidden: false,
		onCardDroped: ()=> {},
	};

	constructor(props){
		super(props);

		this.resetPosition = this.resetPosition.bind(this);
		this.isDropZone = this.isDropZone.bind(this);

		this.state = {
			width: this.props.size,
			height: this.props.size * 1.285714285,

			sizeSymbol: this.props.size * 0.71428571,
			sizeSymbolLittle: this.props.size * 0.285714,
			fontSize: this.props.size * 0.3571428,
			pan: new Animated.ValueXY(),
		}
	}

	resetPosition(){
		Animated.timing(this.state.pan, {
			duration: 250,
			toValue: {x:0,y:0},
		}).start();
	}
	isDropZone(gesture){
	    let dz = this.props.dropZoneValues;
	    if(dz){
	    	  if(gesture.moveY > dz.pageY && gesture.moveY < dz.pageY + dz.height){
	    	  	if(gesture.moveX > dz.pageX && gesture.moveX < dz.pageX + dz.width){
	    	  		 return true;
    	  		}else{
    	  			return false;
    	  		}
	    	  }else{
	    	  	return false;
	    	  }
    	}else{
    		return false;
    	}  
	}

	shouldComponentUpdate(nextProps, nextState){
		return 	this.props.team != nextProps.team || 
				this.props.number != nextProps.number || 
				this.props.hidden != nextProps.hidden ;	
	}

	componentWillMount(){

		const {dragging} = this.props;

		this._panResponder = PanResponder.create({
			onStartShouldSetPanResponder: (evt, gestureState) => dragging,
			onMoveShouldSetPanResponder: (evt, gestureState) => dragging,
			onPanResponderMove: Animated.event([null,{ //Step 3
	            dx : this.state.pan.x,
	            dy : this.state.pan.y,
	            useNativeDriver: true,
       		}]),
       		onPanResponderRelease: (evt, gestureState) => {
       			if(this.isDropZone(gestureState)){
       				this.props.onCardDroped({number: this.props.number, team: this.props.team});

       				//for testing
       				this.resetPosition();
       			}else{
       				this.resetPosition();
       			}
       		}
		});
	}



	render(){
		const {hidden} = this.props;
		const {width, height, fontSize, sizeSymbol, sizeSymbolLittle} = this.state;
		let {team, number} = this.props;
		const color = team == 1 || team == 2 ? RED : BLACK;

		const hiddenStyle  = {opacity: hidden ? 0.0 : 1};



		switch(team){
			case 1:
				team = require('../assets/diamond.png');
			break;
			case 2:
				team = require('../assets/heart.png');
			break;
			case 3:
				team = require('../assets/spade.png');
			break;
			case 4:
				team = require('../assets/trebor.png');
			break;
		}

		switch(number){
			case 1: number = "A"; break;
			case 11: number = "J"; break;
			case 12: number = "Q"; break;
			case 13: number = "K"; break;
		}

		const container = [styles.container, {width, height}, this.state.pan.getLayout(), hiddenStyle];
		const numberStyle = [styles.number, {color, fontSize}];
		const little_symbol = [styles.little_symbol, {width: sizeSymbolLittle, height: sizeSymbolLittle}];
		const symbol = [styles.symbol, {width: sizeSymbol, height: sizeSymbol}];

		return(
			<Animated.View {...this._panResponder.panHandlers} style={[container]} >
				<View  style={styles.header}>
					<Text style={numberStyle}>{number}</Text>
					<Image resizeMode={"contain"} style={little_symbol} source={team}/>
				</View>
				<Image resizeMode={"contain"} style={symbol} source={team}/>
				
			</Animated.View>
		);
	}
}

const styles = {
	container:{
		width: 70,
		height: 90,
		padding:3,
		paddingTop: 0,
		borderRadius: 5,
		elevation: 1,
		shadowOffset:{  width: 10,  height: 10,  },
		shadowColor: 'black',
		shadowOpacity: 1.0,
		backgroundColor: "#fff",
		borderWidth: 1,
		borderColor: "#7C7C7C",
		transform: [{'translate':[0,0,1]}] 
	},
	header:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	number: {
		fontSize: 25,
		fontWeight: 'bold',
	},
	symbol: {
		width: 50,
		height: 50,
		alignSelf: 'center',
	},
	little_symbol:{
		width: 20,
		height: 20,
	}
}


export default Card;
