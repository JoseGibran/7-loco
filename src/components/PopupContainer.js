import React from 'react';
import {Text, View, Animated} from 'react-native';

class PopupContainer extends React.Component{

	constructor(props){
		super(props);
		this.state ={
			scaleAnim: new Animated.Value(1),
		}
	};

	start = () => {
		this.setState({scaleAnim: new Animated.Value(2)}, ()=>{
			Animated.timing(
				this.state.scaleAnim,
				{
					toValue: 1,
					duration: 250
				}
			).start();
		});
	}
	render(){
		const {scaleAnim} = this.state;
		const containerStyle = {transform:[{scaleX: scaleAnim},{scaleY: scaleAnim},]};

		return(
			<Animated.View style={[containerStyle]}>
				{this.props.children}
			</Animated.View>
		);
	}
}

export default PopupContainer;