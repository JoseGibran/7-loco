import React from 'react';
import {View,Text} from 'react-native';

class Loading extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return(
			<View style={styles.backdrop}>
				<Text style={styles.text}>Cargando...</Text>
			</View>
		);
		
	}
}

const styles = {
	backdrop:{
		top:0,
		bottom: 0,
		right:0,
		left:0,
		position: 'absolute',
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		elevation: 4,
	},
	text:{
		fontSize: 18,
		color: "#fff",
	}
};

export default Loading;