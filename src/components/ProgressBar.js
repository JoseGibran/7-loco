import React from 'react';
import {View, Text, Animated,} from 'react-native';


class ProgressBar extends React.Component{

	static defaultProps ={
		time: 5000,
		width: 150,
		onFinish: ()=>{}
	}

	state = {
		widthAnim: new Animated.Value(0),
	};

	constructor(props){
		super(props);
	}

	componentDidMount(){

		const {time, width, onFinish} = this.props;

		Animated.timing(
			this.state.widthAnim,
			{
				toValue: width,
				duration: time, // 5 seg
			}
		).start(onFinish);
	}

	render(){	
		let {widthAnim} = this.state;
		const {width} = this.props;
		return(
			<View style={{
				height: 10,
				width: width,
				borderWidth: 1,
				borderColor: '#7C7C7C',
			}}>
				<Animated.View style={{
					height: 10,
					backgroundColor: '#4F8A10',
					width: widthAnim,
				}}>
					
				</Animated.View>
			</View>
		);
	}
}

export default ProgressBar;