

const SERVER = 'ws://192.168.1.5:3001/';

let instance = null;

class Socket {

	constructor(callback){
		this.onMessageReceived = this.onMessageReceived.bind(this);
		this.ws = new WebSocket(SERVER);
		this.ws.onopen = this.onOpenConnection;
		this.ws.onmessage = this.onMessageReceived;
		this.callback = callback;
	}
	static init(){
		instance = new Socket(()=>{});
	}
	static getInstance(callback){
		instance.callback = callback;
		return instance;
	}
	
	onOpenConnection(){
	    console.log('Open connection!');
	    
	}

	performAction(type, payload){
		let message =  JSON.stringify({type, payload});
		try{
			this.ws.send(message);
		}catch(error){
			alert(error.message);
			this.callback(false);
		}
	}

	onMessageReceived(event){
		let data = JSON.parse(event.data);	
		if(this.callback){
			this.callback(data);
		}
		
	}

	close(){
		this.ws.close();
	}
}

export default Socket;