class Game {

	constructor(players, callback){
		this.players = players;
		this.callback = callback;
		this.unavalibleCards = [];
		this.currentCard = null;
		this.currentTurn = 0;
		this.cardsToPull = 0;
		this.direction = true;
		this.oneDroper = null;

		this.getPlayers = this.getPlayers.bind(this);
		this.getCurrentCard = this.getCurrentCard.bind(this);
		this.startGame = this.startGame.bind(this);
		this.dealCards = this.dealCards.bind(this);
		this.getRandomCard = this.getRandomCard.bind(this);
		this.isCardUnavalible = this.isCardUnavalible.bind(this);
		this.dropCard = this.dropCard.bind(this);
		this.requestCard = this.requestCard.bind(this);
		this.skypeTurn = this.skypeTurn.bind(this);
		this.nextTurn = this.nextTurn.bind(this);

		this.findUserIndex = this.findUserIndex.bind(this);
		this.removeCard = this.removeCard.bind(this);
		this.removeCardFromUnavalible = this.removeCardFromUnavalible.bind(this);
		

		this.dealCards();
	}

	getPlayers(){
		return this.players;
	}
	getCurrentCard(){
		return this.currentCard;
	}

	startGame(){
		const initalCard = this.getRandomCard();
		this.currentCard = initalCard;
		this.unavalibleCards = [...this.unavalibleCards, initalCard];
		this.callback({
			type: "GAME_STARTED",
			result: true,
			data: {
				currentCard: this.currentCard,
				players: this.players,
				currentTurn: this.currentTurn,
			}
		});

	}

	dealCards(){
		this.players.forEach((player)=>{
			player.cards = [];
			for(let i = 0; i < 7; i++){
				let newCard = this.getRandomCard();
				player.cards = [...player.cards, newCard];
				this.unavalibleCards = [...this.unavalibleCards, newCard];
			}
		});

		this.startGame();
	}

	getRandomCard(){
		let result = null;
		let cards = this.unavalibleCards;
		if(this.currentCard){
			cards = [...cards, this.currentCard];
		}
		while(result == null){
			const number = Math.floor(Math.random() * 13) + 1;
			const team = Math.floor(Math.random() * 4) + 1;
			const card = {number, team};

			if(!this.isCardUnavalible(card)){
				result = card;
			}
		}
		return result;
	}

	isCardUnavalible(card){
		let result = false;
		this.unavalibleCards.forEach((value, index) => {
			if(!result){
				if(card.number == value.number && card.team == value.team){
					result = true;
				}
			}
		});

		return result;
	}

	dropCard(payload){
		const {user} = payload;
		let {card} = payload;
		
		card = card || payload.oldCard;
		//PROTOCOL A
		this.oneDroper = card.number === 1 ? user.name : null;

		//PROTOCOL J
		if(card.number === 11){
		}else{
			this.nextTurn();
		}
		
		//PROTOCOL 2
		this.cardsToPull = this.currentCard.number == 2 ? this.cardsToPull + 2 : 0;

		//PROTOCOL Q
		this.direction = !(this.currentCard.number == 12);

		//PROTOCOL 7
		if(card.number === 7){
			const {oldCard, newCard} = payload;
			this.removeCard(user, oldCard);
			this.removeCardFromUnavalible(oldCard);
			this.currentCard = newCard;
		}else{
			this.removeCard(user, card);
			this.removeCardFromUnavalible(card);
			this.currentCard = card;
		}


		this.callback({
			type: "DROP_CARD",
			result: true,
			data: {
				currentCard: this.currentCard,
				players: this.players,
				currentTurn: this.currentTurn,
				cardsToPull: this.cardsToPull,
				oneDroper: this.oneDroper,
			}
		});

		
		if(user.cards.length == 0){
			this.callback({
				type: "GAME_OVER",
				result: true,
				data:{
					winner: user.name,
				}
			});
		}

	}
	requestCard(payload){
		const {user} = payload;
		const indexWillUpdate = this.findUserIndex(user);

		const newCard = this.getRandomCard();

		this.players[indexWillUpdate].cards = [...this.players[indexWillUpdate].cards, newCard];
		this.unavalibleCards = [...this.unavalibleCards, newCard];

		this.callback({
			type: "REQUEST_CARD",
			result: true,
			data: {
				user: user,
				players: this.players,
			}
		});
	}
	skypeTurn(payload){
		const {user} = payload;
		this.nextTurn();
		//PROTOCOL 2
		const indexWillUpdate = this.findUserIndex(user);
		let cardsWillAdd = [];
		for(let i = 0; i < this.cardsToPull; i++){
			cardsWillAdd = [...cardsWillAdd, this.getRandomCard()];
		}
		this.players[indexWillUpdate].cards = [...this.players[indexWillUpdate].cards, ...cardsWillAdd];
		this.cardsToPull = 0;
		this.callback({
			type: "SKYPE_TURN",
			result: true,
			data: {
				currentTurn: this.currentTurn,
				players: this.players,
				cardsToPull: this.cardsToPull,
				oneDroper: this.oneDroper,
			}
		});
	}

	nextTurn(){
		if(this.direction){
			this.currentTurn = this.currentTurn == this.players.length -1 ? 0 : this.currentTurn + 1;
		}else{
			this.currentTurn = this.currentTurn == 0 ? this.players.length -1 : this.currentTurn - 1;
		}
	}

	findUserIndex(user){
		let result = null;
		this.players.forEach((value, index) => {
			if(value.name == user.name){
				result = index;
			}
		});
		return result;
	}
	removeCard(user, card){
		const indexWillUpdate = this.findUserIndex(user);
		let indexWillDelete = null;
		this.players[indexWillUpdate].cards.forEach((value, index) => {
			if(value.number == card.number && value.team == card.team){
				indexWillDelete = index;
			}
		});
		this.players[indexWillUpdate].cards.splice(indexWillDelete, 1);
	}
	removeCardFromUnavalible(card){
		let indexWillDelete = null;
		this.unavalibleCards.forEach((value, index) => {
			if(value.number == card.number && value.team == card.team){
				indexWillDelete = index;
			}
		});
		this.unavalibleCards.splice(indexWillDelete, 1);
	}





}

export default Game;