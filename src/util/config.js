let token = null;

import base64 from './base64';

class Config {
	static setToken(newToken){
		token = newToken;
	}
	static getToken(){
		return token;
	}

	static getCurrentUser(){
		if(token == null){
			return null;
		}else{
			return JSON.parse(base64.b64DecodeUnicode(token));
		}
	}
}

export default Config;