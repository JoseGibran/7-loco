export default styles = {
	navbar:{
		/*navBarHidden: false,
		navBarBackgroundColor: "#34495e",
		navBarComponentAlignment: 'center',
		navBarTextColor: "#fff",*/
	},
	container: {
	    backgroundColor: '#ecf0f1',
	    flex: 1,
  	},
  	input: {
	    height: 40,
	    backgroundColor: '#fff',
  	},
  	inputContainer: {
	    backgroundColor: '#bdc3c7',
	    padding: 5,
  	},
  	button: {
  		paddingLeft: 10,
  		paddingRight: 10,
		height: 50,
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	button_big:{
		width: 180,
		height: 85,
		borderWidth: 1,
		borderColor: '#000',
		alignItems: 'center',
		justifyContent: 'center',
	},
	row: {
		backgroundColor: "#fff",
		borderBottomWidth: 1,
		borderBottomColor: "#F0F0F0",
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: 60,
		paddingLeft: 20,
		paddingRight: 20,
	},
	row_text: {
		fontSize: 16,
	},
}