export default strings = {

	"ok": "Ok",
	"cancel": "Mejor no!",

	"email" : "Email",
	"password": "Password",
	"go": "Go!",
	"invalid_login": "User or pass are incorrect, please try again!",
	"login_error": "Error during Login, contact with the admin",
	"logout_error": "Error durin Logout, concat with the admin",
	"welcome": "7 Loco",
	"logout": "Log out",
	"error_reset_user": "Error al resetear el usuario",
	"async_storage_error": "Error de Guardado Local",

	//DASHBOARD
	'dashboard_title': 'Dashboard',
	'create_match': 'Crear Partida',
	'join_match': "Unirse",
	'chat': 'Chat',
	'games': 'Juegos',
	"create_game_error": "Error al crear Juego",

	//GAMES
	games_title_1: 'Servidor',
	games_title_2: 'Jugadores',
	games_alert_title: 'Unirse!',
	games_alert_body: 'Desea unirse al Juego de: ',
	games_join_error: 'Ocurrió un error al unirse al juego',

	//LOBIES
	lobbie_title: 'Lobbie',
	leave_lobbie: 'Dejar Sala',
	start_game: 'Empezar Juego',
	host: 'Host',
	no_host: 'Unicamente el Host puede iniciar el Juego.',
	no_enough_players: 'Deben haber almenos 2 Jugadores',

	//GAME
	dear: 'Desafiar',
	last_card: 'Ultima',
	current_turn: 'Es turno de:',
	skype_turn: 'Paso',
	invalid_turn: 'No es su turno',
	no_more_cards: 'Ya no hay cartas',
	challenged_by: 'Desafiado por'
}