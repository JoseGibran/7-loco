import { Navigation } from 'react-native-navigation';

import Chat from '../views/Chat';
import Login from '../views/Login';
import Dashboard from '../views/Dashboard';
import Games from '../views/Games';
import Lobbie from '../views/Lobbie';
import Game from '../views/Game';

export default function registerScreens() {
	Navigation.registerComponent('login', ()=> Login);
	Navigation.registerComponent('app.chat', () => Chat);
	Navigation.registerComponent('app.dashboard', ()=>Dashboard);
	Navigation.registerComponent('app.games', ()=>Games);
	Navigation.registerComponent('app.lobbie', ()=>Lobbie);
	Navigation.registerComponent('app.game', ()=>Game);
}